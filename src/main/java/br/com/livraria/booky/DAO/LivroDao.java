package br.com.livraria.booky.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.livraria.booky.model.Livro;

public class LivroDao implements DAO<Livro> {
	private Session session;
	public LivroDao(Session session) {
		this.session = session;
	}
	
	
	public List<Livro> listarTodos() {
		return listarOrdenado("nome");
	}

	
	public Livro listarPorId(int id) {
		return (Livro) session.load(Livro.class, id);
	}

	
	public List<Livro> listarOrdenado(String ordem) {
		return this.session.createCriteria(Livro.class).addOrder(Order.asc(ordem)).list();
	}

	
	public boolean inserir(Livro object) {
		try{
			this.session.save(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean atualizar(Livro object) {
		try{
			this.session.merge(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean remover(Livro object) {
		try{
			this.session.delete(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public void destruir(Livro object) {
		session.evict(object);
	}

	
	public void destruir(List<Livro> lista) {
		session.evict(lista);
	}

}
