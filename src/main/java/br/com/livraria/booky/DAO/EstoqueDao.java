package br.com.livraria.booky.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.livraria.booky.model.Estoque;

public class EstoqueDao implements DAO<Estoque> {
	private Session session;
	
	public EstoqueDao(Session session) {
		this.session = session;
	}
	
	public List<Estoque> listarTodos() {
		return this.session.createCriteria(Estoque.class).addOrder(Order.asc("id")).list();
	}

	
	public Estoque listarPorId(int id) {
		return (Estoque) this.session.load(Estoque.class, id);
	}

	
	public List<Estoque> listarOrdenado(String ordem) {
		return this.session.createCriteria(Estoque.class).addOrder(Order.asc(ordem)).list();
	}

	
	public boolean inserir(Estoque object) {
		try{
			this.session.save(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean atualizar(Estoque object) {
		try{
			this.session.merge(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean remover(Estoque object) {
		try{
			this.session.delete(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public void destruir(Estoque object) {
		session.evict(object);
		
	}

	
	public void destruir(List<Estoque> lista) {
		session.evict(lista);
		
	}

}
