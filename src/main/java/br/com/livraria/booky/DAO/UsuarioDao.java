package br.com.livraria.booky.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.livraria.booky.model.Usuario;

public class UsuarioDao implements DAO<Usuario> {
	private Session session;
	
	public UsuarioDao(Session session) {
		this.session = session;
	}
	
	
	public List<Usuario> listarTodos() {
		return this.session.createCriteria(Usuario.class).addOrder(Order.asc("nome")).list();
	}

	
	public Usuario listarPorId(int id) {
		return (Usuario) this.session.load(Usuario.class, id);
	}
	
	
	public Usuario listarPorLogin(String login) {
		return (Usuario) this.session.load(Usuario.class, login);
	}

	
	public List<Usuario> listarOrdenado(String ordem) {
		return this.session.createCriteria(Usuario.class).addOrder(Order.asc(ordem)).list();
	}

	
	public boolean inserir(Usuario object) {
		try{
			this.session.save(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean atualizar(Usuario object) {
		try{
			this.session.merge(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean remover(Usuario object) {
		try{
			this.session.delete(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public void destruir(Usuario object) {
		session.evict(object);
		
	}

	
	public void destruir(List<Usuario> lista) {
		session.evict(lista);
		
	}

}
