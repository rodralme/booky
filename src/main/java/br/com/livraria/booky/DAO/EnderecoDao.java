package br.com.livraria.booky.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.livraria.booky.model.Endereco;

public class EnderecoDao implements DAO<Endereco> {
	private Session session;
	public EnderecoDao(Session session) {
		this.session = session;
	}
	
	
	public List<Endereco> listarTodos() {
		return this.session.createCriteria(Endereco.class).addOrder(Order.asc("id")).list();
	}

	
	public Endereco listarPorId(int id) {
		return (Endereco) this.session.load(Endereco.class, id);
	}

	
	public List<Endereco> listarOrdenado(String ordem) {
		return this.session.createCriteria(Endereco.class).addOrder(Order.asc(ordem)).list();
	}

	
	public boolean inserir(Endereco object) {
		try{
			this.session.save(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean atualizar(Endereco object) {
		try{
			this.session.merge(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean remover(Endereco object) {
		try{
			this.session.delete(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public void destruir(Endereco object) {
		session.evict(object);
		
	}

	
	public void destruir(List<Endereco> lista) {
		session.evict(lista);
		
	}

}
