package br.com.livraria.booky.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.livraria.booky.model.Editora;

public class EditoraDao implements DAO<Editora> {
	private Session session;
	
	public EditoraDao(Session session) {
		this.session = session;
	}
	
	
	public List<Editora> listarTodos() {
		return this.session.createCriteria(Editora.class).addOrder(Order.asc("id")).list();
	}

	
	public Editora listarPorId(int id) {
		return (Editora) this.session.load(Editora.class, id);
	}

	
	public List<Editora> listarOrdenado(String ordem) {
		return this.session.createCriteria(Editora.class).addOrder(Order.asc(ordem)).list();
	}

	
	public boolean inserir(Editora object) {
		try{
			this.session.save(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean atualizar(Editora object) {
		try{
			this.session.merge(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean remover(Editora object) {
		try{
			this.session.delete(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public void destruir(Editora object) {
		session.evict(object);
		
	}

	
	public void destruir(List<Editora> lista) {
		session.evict(lista);
		
	}

}
