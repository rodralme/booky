package br.com.livraria.booky.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.livraria.booky.model.ItemCompra;

public class ItemCompraDao implements DAO<ItemCompra> {
	private Session session;
	public ItemCompraDao(Session session) {
		this.session = session;
	}
	
	
	public List<ItemCompra> listarTodos() {
		return this.session.createCriteria(ItemCompra.class).addOrder(Order.asc("idItemCompra")).list();
	}

	
	public ItemCompra listarPorId(int id) {
		return (ItemCompra) this.session.load(ItemCompra.class, id);
	}

	
	public List<ItemCompra> listarOrdenado(String ordem) {
		return this.session.createCriteria(ItemCompra.class).addOrder(Order.asc(ordem)).list();
	}

	
	public boolean inserir(ItemCompra object) {
		try{
			this.session.save(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean atualizar(ItemCompra object) {
		try{
			this.session.merge(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean remover(ItemCompra object) {
		try{
			this.session.delete(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public void destruir(ItemCompra object) {
		session.evict(object);
		
	}

	
	public void destruir(List<ItemCompra> lista) {
		session.evict(lista);
		
	}

}
