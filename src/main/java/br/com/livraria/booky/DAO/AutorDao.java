package br.com.livraria.booky.DAO;

import java.util.List;

import org.hibernate.Session;

import br.com.livraria.booky.model.Autor;

public class AutorDao {
	private Session session;
	public AutorDao(Session session){
		this.session = session;
	}
	
	public List<Autor> listarTodos() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Autor listarPorId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public List<Autor> listarOrdenado(String ordem) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public boolean inserir(Autor object) {
		try{
			this.session.save(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	
	public boolean atualizar(Autor object) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean remover(Autor object) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public void destruir(Autor object) {
		// TODO Auto-generated method stub
		
	}

	
	public void destruir(List<Autor> lista) {
		// TODO Auto-generated method stub
		
	}

}
