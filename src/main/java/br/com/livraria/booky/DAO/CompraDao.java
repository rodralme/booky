package br.com.livraria.booky.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.livraria.booky.model.Compra;

public class CompraDao{
	private Session session;
	
	public CompraDao(Session session) {
		this.session = session;
	}
	public List<Compra> listarTodos() {
		return this.session.createCriteria(Compra.class).addOrder(Order.asc("id")).list();
	}

	public Compra listarPorId(int id) {
		return (Compra) this.session.load(Compra.class, id);
	}

	public List<Compra> listarOrdenado(String ordem) {
		return this.session.createCriteria(Compra.class).addOrder(Order.asc(ordem)).list();
	}

	public boolean inserir(Compra object) {
		try{
			this.session.save(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	public boolean atualizar(Compra object) {
		try{
			this.session.merge(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	public boolean remover(Compra object) {
		try{
			this.session.delete(object);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	public void destruir(Compra object) {
		session.evict(object);
		
	}

	public void destruir(List<Compra> lista) {
		session.evict(lista);
		
	}

}
