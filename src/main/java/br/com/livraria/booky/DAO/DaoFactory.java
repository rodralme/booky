package br.com.livraria.booky.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.livraria.booky.conexao.HibernateConexao;

public class DaoFactory {
	private final Session session;
	private Transaction transaction;

	public DaoFactory() {
		if (hasTransaction()) {
			session = null;
			stopTransaction();
		} else {
			session = HibernateConexao.getSession();
		}
	}

	public void beginTransaction() {
		this.transaction = this.session.beginTransaction();
	}

	public void comit() {
		this.transaction.commit();
		this.transaction = null;
	}

	public boolean hasTransaction() {
		return this.transaction != null;
	}

	public void rollBack() {
		this.transaction.rollback();
		this.transaction = null;
	}

	public void stopTransaction() {
		this.transaction = null;
	}

	public void close() {
		this.session.close();
	}

	public void clear() {
		this.session.clear();
	}
	
//	public DepartamentoDao getDepartamentoDao(){
//		return new DepartamentoDao(this.session);
//	}
	
	public EnderecoDao getEnderecoDao() {
		return new EnderecoDao(session);
	}

	public UsuarioDao getUsuarioDao() {
		return new UsuarioDao(session);
	}

	public AutorDao getAutorDao() {
		return new AutorDao(session);
	}

	public LivroDao getLivro() {
		return new LivroDao(session);
	}

	public EditoraDao getEditora() {
		// TODO Auto-generated method stub
		return new EditoraDao(session);
	}
}
