package br.com.livraria.booky.DAO;

import java.util.List;

public interface DAO<Object> {
	
	public List<Object> listarTodos();
	public Object listarPorId(int id);
	public List<Object> listarOrdenado(String ordem);
	public boolean inserir(Object object);
	public boolean atualizar(Object object);
	public boolean remover(Object object);
	public void destruir(Object object);
	public void destruir(List<Object> lista);
	
}
