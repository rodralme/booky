package br.com.livraria.booky.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="usuario")
public class Usuario {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idUsuario")
	private int idUsuario;
	private String nome;
	private BigInteger cpf;
	private BigInteger rg;
	private String email;
	private BigInteger telefone;
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEndereco")
	@Cascade(CascadeType.ALL)
	private Endereco endereco;
	private String login;
	private String senha;
	private int tipoAdministrador;
	
	public final int getIdUsuario() {
		return idUsuario;
	}
	public final void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public final String getNome() {
		return nome;
	}
	public final void setNome(String nome) {
		this.nome = nome;
	}
	public final BigInteger getCpf() {
		return cpf;
	}
	public final void setCpf(BigInteger cpf) {
		this.cpf = cpf;
	}
	public final BigInteger getRg() {
		return rg;
	}
	public final void setRg(BigInteger rg) {
		this.rg = rg;
	}
	public final String getEmail() {
		return email;
	}
	public final void setEmail(String email) {
		this.email = email;
	}
	public final BigInteger getTelefone() {
		return telefone;
	}
	public final void setTelefone(BigInteger telefone) {
		this.telefone = telefone;
	}
	public final Endereco getEndereco() {
		return endereco;
	}
	public final void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public final String getLogin() {
		return login;
	}
	public final void setLogin(String login) {
		this.login = login;
	}
	public final String getSenha() {
		return senha;
	}
	public final void setSenha(String senha) {
		this.senha = senha;
	}
	public final int getTipoAdministrador() {
		return tipoAdministrador;
	}
	public final void setTipoAdministrador(int tipoAdministrador) {
		this.tipoAdministrador = tipoAdministrador;
	}
	
	
}
