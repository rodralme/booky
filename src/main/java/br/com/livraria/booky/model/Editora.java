package br.com.livraria.booky.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="editora")
public class Editora {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idEditora")
	private int idEditora;
	private String nome;
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEndereco")
	@Cascade(CascadeType.ALL)
	private Endereco endereco;
	@OneToMany(fetch=FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	private List<Livro> listaLivros;
	
	public final int getIdEditora() {
		return idEditora;
	}
	public final void setIdEditora(int idEditora) {
		this.idEditora = idEditora;
	}
	public final String getNome() {
		return nome;
	}
	public final void setNome(String nome) {
		this.nome = nome;
	}
	public final Endereco getEndereco() {
		return endereco;
	}
	public final void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public final List<Livro> getListaLivros() {
		return listaLivros;
	}
	public final void setListaLivros(List<Livro> listaLivros) {
		this.listaLivros = listaLivros;
	}
	
	
}
