package br.com.livraria.booky.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="endereco")
public class Endereco {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idEndereco")
	private int idEndereco;
	private String logradouro;
	private String complemento;
	private String bairro;
	private String cidade;
	private String estado;
	private BigInteger cep;
	
	public final int getIdEndereco() {
		return idEndereco;
	}
	public final void setIdEndereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}
	public final String getLogradouro() {
		return logradouro;
	}
	public final void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public final String getComplemento() {
		return complemento;
	}
	public final void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public final String getBairro() {
		return bairro;
	}
	public final void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public final String getCidade() {
		return cidade;
	}
	public final void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public final String getEstado() {
		return estado;
	}
	public final void setEstado(String estado) {
		this.estado = estado;
	}
	public final BigInteger getCep() {
		return cep;
	}
	public final void setCep(BigInteger cep) {
		this.cep = cep;
	}
	
}
