package br.com.livraria.booky.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="estoque")
public class Estoque {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idEstoque")
	private int idEstoque;
	
	private BigInteger quantidade;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idLivro")
	private Livro livro;
	
	public final int getIdEstoque() {
		return idEstoque;
	}
	public final void setIdEstoque(int idEstoque) {
		this.idEstoque = idEstoque;
	}
	public final BigInteger getQuantidade() {
		return quantidade;
	}
	public final void setQuantidade(BigInteger quantidade) {
		this.quantidade = quantidade;
	}
	public final Livro getLivro() {
		return livro;
	}
	public final void setLivro(Livro livro) {
		this.livro = livro;
	}
	
	
}
