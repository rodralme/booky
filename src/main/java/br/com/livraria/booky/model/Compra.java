package br.com.livraria.booky.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="compra")
public class Compra {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idCompra")
	private int idCompra;
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idUsuario")
	private Usuario usuario;
	private Calendar data;
	
	public final int getIdCompra() {
		return idCompra;
	}
	public final void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}
	public final Usuario getUsuario() {
		return usuario;
	}
	public final void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public final Calendar getData() {
		return data;
	}
	public final void setData(Calendar data) {
		this.data = data;
	}
}