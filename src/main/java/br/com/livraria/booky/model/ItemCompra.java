package br.com.livraria.booky.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="item_compra")
public class ItemCompra {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idItemCompra")
	private int idItemCompra;
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idCompra")
	private Compra compra;
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idLivro")
	private Livro livro;
	private int quantidade;
	
	public final int getIdItemCompra() {
		return idItemCompra;
	}
	public final void setIdItemCompra(int idItemCompra) {
		this.idItemCompra = idItemCompra;
	}
	public final Compra getCompra() {
		return compra;
	}
	public final void setCompra(Compra compra) {
		this.compra = compra;
	}
	public final Livro getLivro() {
		return livro;
	}
	public final void setLivro(Livro livro) {
		this.livro = livro;
	}
	public final int getQuantidade() {
		return quantidade;
	}
	public final void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
}