package br.com.livraria.booky.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController {
	
	@RequestMapping("/index")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("index");
		mv.addObject("mensagem", "Teste");
		
		return mv;
	}
	
	@RequestMapping("/teste")
	public ModelAndView teste() {
		ModelAndView mv = new ModelAndView("teste");
		mv.addObject("mensagem", "Teste OK!!!");
		
		return mv;
	}

}
