package br.com.livraria.booky.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.livraria.booky.action.Action;
import br.com.livraria.booky.action.ActionFactory;

/**
 * Servlet implementation class FrontController
 */
@WebServlet("/FrontController")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		Action actionObject = null;
		if (action == null) {
			response.sendRedirect("Erro404.xhtml");
		}
		actionObject = ActionFactory.create(action);
		if (actionObject != null) {
			actionObject.execute(request, response);
		}
    	
    	//DaoFactory daoFactory = new DaoFactory();
    	//EnderecoDao enderecoDao = daoFactory.getEnderecoDao();
    	//List<Endereco> listarTodos = enderecoDao.listarTodos();
    	
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
