package br.com.livraria.booky.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.livraria.booky.DAO.DaoFactory;
import br.com.livraria.booky.DAO.LivroDao;
import br.com.livraria.booky.DAO.UsuarioDao;
import br.com.livraria.booky.model.Autor;
import br.com.livraria.booky.model.Livro;
import br.com.livraria.booky.model.Usuario;

@Controller
public class LivroController {
	
	@RequestMapping("/livro")
	public ModelAndView listar() {
		
		DaoFactory daoFactory = new DaoFactory();
		UsuarioDao usuarioDao = daoFactory.getUsuarioDao();
		List<Usuario> listarTodos = usuarioDao.listarTodos();
		
		ModelAndView mv = new ModelAndView("livro");
		List<Livro> livros = new ArrayList<Livro>();
		Livro livro = null;
		
		Autor autor = null;
		autor = new Autor();
		autor.setNome("J�lio");
		autor.setSobrenome("Verne");
		
		livro = new Livro();
		livro.setNome("Volta Ao Mundo Em Oitenta Dias");
		livro.setAutor(autor);
		livros.add(livro);
		
		livro = new Livro();
		livro.setIsbn(9788562525162L);
		livro.setNome("Vinte Mil L�guas Submarinas");
		livro.setDescricao("Um misterioso monstro vem assombrando os oceanos. Destruindo navios e matando seua tripulantes. Publicado originalmente em 1870, Vinte mil l�guas submarinas � um exemplo cl�ssico da vis�o e da imagina��o de J�lio Verne. O pioneirismo de sua escrita fez com que fosse reconhecido como o pai da fic��o cient�fica.");
		livro.setValor(new BigDecimal(27.9));
		livro.setAutor(autor);
		livros.add(livro);
		
		mv.addObject("livros", livros);
		return mv;
	}

}
