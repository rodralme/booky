package br.com.livraria.booky.action;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.livraria.booky.DAO.AutorDao;
import br.com.livraria.booky.DAO.DaoFactory;
import br.com.livraria.booky.DAO.EditoraDao;
import br.com.livraria.booky.DAO.EnderecoDao;
import br.com.livraria.booky.DAO.LivroDao;
import br.com.livraria.booky.model.Autor;
import br.com.livraria.booky.model.Editora;
import br.com.livraria.booky.model.Endereco;
import br.com.livraria.booky.model.Livro;

public class CadastrarLivroAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) {
		DaoFactory daoFactory = new DaoFactory();
		
		Endereco endereco = new Endereco();
		EnderecoDao enderecoDao = daoFactory.getEnderecoDao();
		endereco.setLogradouro(request.getParameter("logradouro"));
		endereco.setComplemento(request.getParameter("complemento"));
		endereco.setBairro(request.getParameter("bairro"));
		endereco.setCidade(request.getParameter("cidade"));
		endereco.setEstado(request.getParameter("estado"));
		endereco.setCep(BigInteger.valueOf(Integer.parseInt(request.getParameter("cep"))));
		
		boolean insercaoEndereco = enderecoDao.inserir(endereco);
		if (insercaoEndereco==true)
		{
			//Endereco Inserido com Sucesso
			Editora editora = new Editora();
			EditoraDao editoraDao = daoFactory.getEditora();
			editora.setNome(request.getParameter("nomeEditora"));
			editora.setEndereco(endereco);
			//editora.setListaLivros(listaLivros);
			
			boolean insercaoEditora = editoraDao.inserir(editora);
			if (insercaoEditora==true)
			{			
			Autor autor = new Autor();
			AutorDao autorDao = daoFactory.getAutorDao();
			autor.setNome(request.getParameter("nomeAutor"));
			autor.setSobrenome(request.getParameter("sobrenomeAutor"));
			
			boolean insercaoAutor = autorDao.inserir(autor);
			if (insercaoAutor==true)
			{
				//Autor Inserido com Sucesso
				Livro livro = new Livro();
				LivroDao livroDao = daoFactory.getLivro();
				livro.setIsbn(Long.parseLong(request.getParameter("isbn")));
				livro.setNome(request.getParameter("titulo"));
				livro.setDescricao(request.getParameter("descricao"));
				livro.setValor(BigDecimal.valueOf(Double.parseDouble(request.getParameter("valor"))));
				livro.setImagem(request.getParameter("caminhoImagem"));
				livro.setAutor(autor);
				livro.setEditora(editora);
				
				boolean insercaoLivro = livroDao.inserir(livro);
				if (insercaoLivro==true)
				{
					//Livro Inserido com sucesso
					try {
						response.sendRedirect("index.xhtml");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					//Erro ao inserir o livro
					try {
						response.sendRedirect("CadastroLivro.xhtml");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			else
			{
				//Erro ao inserir o autor
				try {
					response.sendRedirect("CadastroLivro.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else
		{
			//Erro ao inserir Editora
			try {
				response.sendRedirect("CadastroLivro.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}
		else
		{
			//Erro ao inserir o endereco da editora
			try {
				response.sendRedirect("CadastroLivro.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}