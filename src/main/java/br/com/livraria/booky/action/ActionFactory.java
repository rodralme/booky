package br.com.livraria.booky.action;



public class ActionFactory {
	public static Action create(String action) {
		String nomeClasse = "br.com.livraria.booky.action." + action + "Action";
		Class<?> classe = null;
		Object object = null;
		
		try {
			classe = Class.forName(nomeClasse);
			object = classe.newInstance();
		} catch (Exception e) {
			return null;
		}
		if (object instanceof Action) {
			return (Action) object;
		}
		return null;
	}
}
