package br.com.livraria.booky.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.fabric.Response;

import br.com.livraria.booky.DAO.DaoFactory;
import br.com.livraria.booky.DAO.UsuarioDao;
import br.com.livraria.booky.model.Usuario;

public class ValidarLoginAction implements Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) {
		Usuario usuario = new Usuario();
		usuario.setLogin(request.getParameter("login"));
		usuario.setSenha(request.getParameter("senha"));
		
		DaoFactory daoFactory = new DaoFactory();
		UsuarioDao usuarioDao = daoFactory.getUsuarioDao();
		
		Usuario selectLogin = usuarioDao.listarPorLogin(usuario.getLogin());
		if ((selectLogin.getLogin()==usuario.getLogin()) && (selectLogin.getSenha()==usuario.getSenha()))
		{
			//Usuario Cadastrado
			HttpSession sessao = request.getSession();
			sessao.setAttribute("usuario", usuario);
			RequestDispatcher rd = request.getRequestDispatcher("index.xhtml");
		}
		else 
		{
			//Usuario n�o Cadastrado
			try {
				response.sendRedirect("index.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
