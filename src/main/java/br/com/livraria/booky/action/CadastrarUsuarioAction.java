package br.com.livraria.booky.action;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.livraria.booky.DAO.DaoFactory;
import br.com.livraria.booky.DAO.EnderecoDao;
import br.com.livraria.booky.DAO.UsuarioDao;
import br.com.livraria.booky.model.Endereco;
import br.com.livraria.booky.model.Usuario;

public class CadastrarUsuarioAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) {
		DaoFactory daoFactory = new DaoFactory();
		
		Endereco endereco = new Endereco();
		EnderecoDao enderecoDao = daoFactory.getEnderecoDao();
		endereco.setLogradouro(request.getParameter("logradouro"));
		endereco.setComplemento(request.getParameter("complemento"));
		endereco.setBairro(request.getParameter("bairro"));
		endereco.setCidade(request.getParameter("cidade"));
		endereco.setEstado(request.getParameter("estado"));
		endereco.setCep(BigInteger.valueOf(Integer.parseInt(request.getParameter("cep"))));
		
		boolean insercaoEndereco = enderecoDao.inserir(endereco);
		if (insercaoEndereco==true)
		{
			//Endereco inserido com sucesso
			Usuario usuario = new Usuario();
			UsuarioDao usuarioDao = daoFactory.getUsuarioDao();
			usuario.setNome(request.getParameter("nomeCompleto"));
			usuario.setCpf(BigInteger.valueOf(Integer.parseInt(request.getParameter("cpf"))));
			usuario.setRg(BigInteger.valueOf(Integer.parseInt(request.getParameter("rg"))));
			usuario.setEmail(request.getParameter("email"));
			usuario.setTelefone(BigInteger.valueOf(Integer.parseInt(request.getParameter("telefone"))));
			usuario.setLogin(request.getParameter("login"));
			usuario.setSenha(request.getParameter("senha"));
			usuario.setEndereco(endereco);
			
			boolean insercaoUsuario = usuarioDao.inserir(usuario);
			if (insercaoUsuario==true)
			{
				//Usuario Inserido com Sucesso
				try {
					response.sendRedirect("index.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				//Erro ao inserir o usuario
				try {
					response.sendRedirect("CadastroUsuario.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else
		{
			//Erro ao inserir o endere�o do usuario
			try {
				response.sendRedirect("CadastroUsuario.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}

}
