package br.com.livraria.booky.conexao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import br.com.livraria.booky.model.Autor;
import br.com.livraria.booky.model.Compra;
import br.com.livraria.booky.model.Editora;
import br.com.livraria.booky.model.Endereco;
import br.com.livraria.booky.model.ItemCompra;
import br.com.livraria.booky.model.Livro;
import br.com.livraria.booky.model.Usuario;


public class HibernateConexao {
	private static final String LIVRARIA_BOOKS = "livraria_books";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "";
	private static final String HOST = "localhost";
	
	
	private static SessionFactory factory;
	static {
		Configuration conf = new Configuration();
		conf.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		conf.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
		conf.setProperty("hibernate.connection.url", "jdbc:mysql://" + HOST + ":3306/" + LIVRARIA_BOOKS + "?autoReconnect=true");
		conf.setProperty("hibernate.connection.username", USERNAME);
		conf.setProperty("hibernate.connection.password", PASSWORD);
		conf.setProperty("hibernate.default_schema", LIVRARIA_BOOKS);
		conf.setProperty("hibernate.hbm2ddl.auto", "update");
		
		//Configurações de Debug
		conf.setProperty("hibernate.show_sql", "true");
		conf.setProperty("hibernate.use_sql_comment", "false");
		conf.setProperty("hibernate.generate_statistics", "false");
		conf.setProperty("format_sql", "false");

		
		//Mapeando as classes anotadas
		conf.addAnnotatedClass(Endereco.class);
		conf.addAnnotatedClass(Usuario.class);
		conf.addAnnotatedClass(Editora.class);
		conf.addAnnotatedClass(Livro.class);
		conf.addAnnotatedClass(ItemCompra.class);
		//conf.addAnnotatedClass(Estoque.class);
		conf.addAnnotatedClass(Autor.class);
		conf.addAnnotatedClass(Compra.class);
		
		// mudei a forma de instanciar o SessionFactory pq estou usando o hibernate 4
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(conf.getProperties());
		factory = conf.buildSessionFactory(builder.build());
	}
	
	public static Session getSession() {
		return factory.openSession();
	}
}
